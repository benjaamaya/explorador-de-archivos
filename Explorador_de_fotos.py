#Explorador_de_fotos.py
import os
from os.path import *
import tkinter as tk
from tkinter.ttk import *
from tkinter.constants import BOTH, END, LEFT, RIGHT, X, Y
from tkinter.filedialog import Directory, askdirectory
from tkinter import Frame, Listbox, Scrollbar, ttk, messagebox, filedialog
from centrar_ventana import centrar
import shutil
import glob

class Application(ttk.Frame):

    def __init__(self, main_window):
        super().__init__(main_window)
        
        #se crea la ventana principal
        main_window.title('Copiar archivos')
        
        #se crea el cuadro numero 1
        self.frame1 = ttk.Frame(self)
        self.frame1.grid(padx=5, pady=5, row=0, column=0)
        
        #se crea el boton origen
        self.button1 = ttk.Button(self.frame1, command=self.obtener_origen, text='Origen')
        self.button1.grid(padx=5, pady=5, row=0, column=0)
        

        self.entry1 = ttk.Entry(self.frame1)
        self.entry1.grid(padx=5, pady=5, row=0, column=1)

        #se crea el cuadro numero 2
        self.frame2 = ttk.Frame(self)
        self.frame2.grid(padx=5, pady=5, row=0, column=1)
        
        #se crea el boton destino
        self.button2 = ttk.Button(self.frame2, command=self.obtener_destino, text='Destino')
        self.button2.grid(padx=5, pady=5, row=0, column=0)

        self.entry2 = ttk.Entry(self.frame2)
        self.entry2.grid(padx=5, pady=5, row=0, column=1)

        self.frame3 = ttk.Frame(self)
        self.frame3.grid(padx=5, pady=5, row=1, column=0, columnspan=2)
        
        #se crea el cuadro de filtro
        self.filtro = tk.Text()
        self.filtro.insert(tk.INSERT, '*.*')
        self.entry3 = ttk.Entry(self.frame3, textvariable=self.filtro)
        self.entry3.grid(padx=5, pady=5, row=1, column=0)

        #se crea el boton filtrar
        self.button3 = ttk.Button(self.frame3, command=self.filtrar, text='Filtrar')
        self.button3.grid(padx=5,pady=5, row=1, column=1)
        
        #se le da el nombre a los listbox
        self.label1 = ttk.Label(self, text='Origen')
        self.label1.grid(padx=5, pady=5, row=3, column=0)
        self.label2 = ttk.Label(self, text='Destino')
        self.label2.grid(padx=5, pady=5, row=3, column=1)
        self.source_files = tk.StringVar
        
        #se crean los listbox donde se mostraran los elementos de las carpetas
        #listbox 1
        self.listbox1 = tk.Listbox(self, width=45, height=20)
        self.listbox1.grid(padx=5, pady=5, row=4, column=0)
        scrollbar = ttk.Scrollbar(root)
        scrollbar.pack(side= RIGHT, fill=Y)
        self.listbox1.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.listbox1.yview)
        
        #listbox 2
        self.listbox2 = tk.Listbox(self, width=45, height=20)
        self.listbox2.grid(padx=5, pady=5, row=4, column=1)

        #se crea el boton inciar copia
        self.button4 = ttk.Button(self, command=self.iniciar_copia, text='Iniciar copia')
        self.button4.grid(sticky=tk.E ,padx=5, pady=5, row=5, column=1)


        #se empaquetan todos los componentes de la ventana
        self.pack()

    #la funcion filtrar se encarga de filtrar y mostrar solamente los archivos con las extensiones solicitadas
    def filtrar(self):
        messagebox.showinfo(message='Filtrar', title='Filtrar')

    #la funcion obtener_origen se encarga de seleccionar la carpeta (en este caso de origen) y listar todos
    #sus elementos
    def obtener_origen(self):
        origen = filedialog.askdirectory(initialdir="/")
        self.source_files= os.listdir(origen)
        for x in self.source_files:
            self.listbox1.insert(0, x)
            
            
    #la funcion obtener_destino se encarga de seleccionar la carpeta (en este caso de destino) y listar todos
    #sus elementos
    def obtener_destino(self):
        destino = filedialog.askdirectory(initialdir="/")
        self.source_files= os.listdir(destino)
        for x in self.source_files:
            self.listbox2.insert(0, x)

    #la funcion iniciar copia se encarga de copiar el elemento seleccionados a la carpeta de destino
    def iniciar_copia(self):
        ruta = os.getcwd() + os.sep
        origen = self.listbox1.curselection()
        destino = self.listbox2.curselection()
        try:
            shutil.copy(origen, destino)
            messagebox.showinfo(message='Copiado',title = '¡Exitoso!')
        except:
            messagebox.showinfo(message='Error en la copia',title = 'Error')


if __name__ == '__main__':
    root = tk.Tk()
    root.geometry(centrar(ancho=800, alto=580, app=root))
    root.resizable(0,0)
    app = Application(root)
    root.mainloop()